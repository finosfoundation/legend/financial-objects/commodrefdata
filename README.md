[![FINOS - Forming](https://cdn.jsdelivr.net/gh/finos/contrib-toolbox@master/images/badge-forming.svg)](https://github.com/finos/community/blob/master/governance/Software-Projects/Project-Lifecycle.md#forming-projects-optional)

# CML Reference Data Standard

To do: Short blurb about what your standard does.

## Background 

## Roadmap

To do: List the roadmap steps; alternatively link the Confluence Wiki page where the project roadmap is published.

1. Item 1
2. Item 2
3. ....

# Get Involved: Contribute to the CML Reference Data Standard
There are several ways to contribute to the CML Reference Data standard:

* **Join the next meeting**:

To do: Set meetings

* **Join the mailing list**: 

To do: Create mailing list if needed.

* **Raise an issue**: 
If you have any questions or suggestions, please [raise an issue](https://gitlab.com/finosfoundation/legend/financial-objects/commodrefdata/-/issues?sort=created_date&state=opened)

## License

This project uses the **Community Specification License 1.0** ; you can read more in the [LICENSE](LICENSE) file.
